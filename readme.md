## Lumen PHP Framework

I have chosen Lumen as I'm familiar with Laravel eco system and I use Lumen for my personal API project and I've tested it could handle a lot of request per second 

I have decided to do not use any external package and build some of them with small amount of features only to use on this test.

Instructions:

1) Clone the project in a new folder

2) Hit the root with a get request, it will initialize the csv. To make it easy to store, I am just using Cache Facade and then store an encoded version.

Available routes

GET /v1/recipes  | to list all the recipes default limit = 2

GET /v1/recipes?category=(array of categories, it will filter by box_type)

POST /v1/recipes | only two required parameters, slug and title, dynamic id
 
