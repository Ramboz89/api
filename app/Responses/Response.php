<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 27/04/16
 * Time: 12:44
 */

namespace App\Responses;

abstract class Response {

    protected $statusCode;
    protected $body;

    function __construct($body, $statusCode = 200)
    {
        $this->statusCode = $statusCode;
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public abstract function send();
}