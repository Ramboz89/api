<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 26/04/16
 * Time: 19:34
 */

namespace App\Responses;


class JsonResponse extends Response
{
    public function send()
    {
        return response()->json($this->getBody(), $this->getStatusCode());
    }


}