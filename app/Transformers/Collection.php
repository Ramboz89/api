<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 29/04/16
 * Time: 01:20
 */

namespace App\Transformers;


class Collection extends Resource
{
    protected $paginator;

    public function transform()
    {
        foreach ($this->data as $key => $row) {
            $this->data[$key] = $this->transformer->transform($row);
        }
        return $this;
    }

    public function addPagination()
    {
        $this->data['cursor'] = 'Needs to be implemented';

        return $this;
    }
}