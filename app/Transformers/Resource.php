<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 29/04/16
 * Time: 01:20
 */

namespace App\Transformers;


use App\Interfaces\ITransformer;

abstract class Resource
{
    protected $transformer;

    protected $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return ITransformer
     */
    public function getTransformer()
    {
        return $this->transformer;
    }

    /**
     * @param ITransformer $transformer
     */
    public function setTransformer($transformer)
    {
        $this->transformer = $transformer;
    }

    public function __construct($data, ITransformer $transformer)
    {
        $this->data = $data;
        $this->transformer = $transformer;
    }

    public abstract function transform();
} 