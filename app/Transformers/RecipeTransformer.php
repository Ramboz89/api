<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 29/04/16
 * Time: 01:05
 */

namespace App\Transformers;


use App\Interfaces\ITransformer;

class RecipeTransformer implements ITransformer
{

    public function transform(array $attributes)
    {
        return [
            'id' => (int)$attributes['id'],
            "created_at" => $attributes["created_at"],
            "updated_at" => $attributes["updated_at"],
            "box_type" => $attributes["box_type"],
            "title" => $attributes["title"],
            "slug" => $attributes["slug"],
            "short_title" => $attributes["short_title"],
            "marketing_description" => $attributes["marketing_description"],
            "calories_kcal" => (int)$attributes['calories_kcal'],
            "protein_grams" => (int)$attributes["protein_grams"],
            "fat_grams" => (int)$attributes["fat_grams"],
            "carbs_grams" => (int)$attributes["carbs_grams"],
            "bulletpoints" => [
                $attributes['bulletpoint1'],
                $attributes['bulletpoint2'],
                $attributes['bulletpoint3'],
            ],
            "recipe_diet_type_id" => (int)$attributes["recipe_diet_type_id"],
            "season" => $attributes["season"],
            "base" => $attributes["base"],
            "protein_source" => $attributes["protein_source"],
            "preparation_time_minutes" => (int)$attributes["preparation_time_minutes"],
            "shelf_life_days" => (int)$attributes["shelf_life_days"],
            "equipment_needed" => $attributes["equipment_needed"],
            "origin_country" => $attributes["origin_country"],
            "recipe_cuisine" => $attributes["recipe_cuisine"],
            "in_your_box" => $attributes["in_your_box"],
            "gousto_reference" => (int)$attributes["gousto_reference"],
            'links' => [
                [
                    'rel' => 'self',
                    'uri' => '/v1/receipes/' . $attributes['id'],
                ]
            ]
        ];
    }
}