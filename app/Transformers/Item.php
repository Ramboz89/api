<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 29/04/16
 * Time: 02:00
 */

namespace App\Transformers;


class Item extends Resource 
{
    public function transform()
    {
        $this->data = $this->transformer->transform($this->data);

        return $this;
    }
} 