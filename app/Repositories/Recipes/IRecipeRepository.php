<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 26/04/16
 * Time: 17:12
 */

namespace App\Repositories\Recipes;


use App\Model;

interface IRecipeRepository
{
    public function find($id);
    public function findAll(array $filters);
    public function update(Model $model, array $data);
    public function store(array $data);
    public function rate(Model $model, $stars);

}