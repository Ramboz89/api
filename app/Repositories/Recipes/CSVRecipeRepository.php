<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 26/04/16
 * Time: 17:19
 */

namespace App\Repositories\Recipes;


use App\Model;
use App\Transformers\Collection;
use App\Transformers\Item;
use App\Transformers\RecipeTransformer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CSVRecipeRepository implements IRecipeRepository
{

    private $model;

    function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        try {
            $recipe = $this->model->find($id);
        } catch (NotFoundHttpException $e) {
            throw $e;
        }
        //all this new response object could be moved into a factory and chose which kind of response return
        return $this->getTransformedItem($recipe);
        // TODO: Implement get() method.
    }
    public function findAll(array $filters)
    {
        try{
            $recipes = $this->model->all($filters);
        } catch(NotFoundHttpException $e){
            throw $e;
        }
        $transformed = new Collection($recipes, new RecipeTransformer());
        return $transformed->transform()
            ->addPagination()
            ->getData();
    }

    public function update(Model $model, array $data)
    {
        // TODO: Implement update() method.
    }

    public function store(array $data)
    {
        try {
            $recipe = $this->model->create($data);
        } catch (\Exception $e) {
            throw $e;
        }
        return $this->getTransformedItem($recipe);
    }

    public function rate(Model $model, $stars)
    {
        // TODO: Implement rate() method.
    }

    /**
     * @param $recipe
     * @return mixed
     */
    private function getTransformedItem($recipe)
    {
        $transformed = new Item($recipe->getAttributes(), new RecipeTransformer());
        $recipe = $transformed->transform()
            ->getData();
        return $recipe;
    }
}