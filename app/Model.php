<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 27/04/16
 * Time: 13:00
 */

namespace App;


use App\Traits\FilterableTrait;
use App\Traits\PaginableTrait;
use Illuminate\Contracts\Support\Jsonable;

abstract class Model implements Jsonable{
    use FilterableTrait, PaginableTrait;

    protected $fillable;
    protected $attributes;
    protected $hidden;

    function __construct($attributes)
    {
        $this->fill($attributes);
    }

    public function fill($attributes)
    {
        if(!empty($attributes)){
            foreach ($this->attributes as $key => $value) {
                $this->attributes[$key] = array_get($attributes, $key, '');
            }
        }
    }

    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    public abstract function toJson($options = 0);
    public abstract function find($id);
    public abstract function create($data);
    public abstract function update($data);



}