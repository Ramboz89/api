<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 28/04/16
 * Time: 14:08
 */

namespace App\Traits;


trait PaginableTrait
{

    public function applyPagination($collection, $page, $offset)
    {
        return array_slice($collection, $page * $offset, $offset );
    }
}