<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 27/04/16
 * Time: 16:02
 */

namespace App\Traits;


use App\Filters\QueryFilter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait FilterableTrait
{

    public function applyFilters($collection, QueryFilter $filters)
    {
        $collection = $filters->apply($collection);
        if(count($collection) == 0) {
            throw new NotFoundHttpException();
        }
        return $collection;
    }

}