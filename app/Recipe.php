<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 26/04/16
 * Time: 17:36
 */

namespace App;

use App\Filters\RecipeFilter;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class Recipe extends Model
{

    protected $attributes = [
        "id" => "",
        "created_at" => "",
        "updated_at" => "",
        "box_type" => "",
        "title" => "",
        "slug" => "",
        "short_title" => "",
        "marketing_description" => "",
        "calories_kcal" => "",
        "protein_grams" => "",
        "fat_grams" => "",
        "carbs_grams" => "",
        "bulletpoint1" => "",
        "bulletpoint2" => "",
        "bulletpoint3" => "",
        "recipe_diet_type_id" => "",
        "season" => "",
        "base" => "",
        "protein_source" => "",
        "preparation_time_minutes" => "",
        "shelf_life_days" => "",
        "equipment_needed" => "",
        "origin_country" => "",
        "recipe_cuisine" => "",
        "in_your_box" => "",
        "gousto_reference" => "",
    ];

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    protected $fillable = [
        "created_at",
        "updated_at",
        "box_type",
        "title",
        "slug",
        "short_title",
        "marketing_description",
        "calories_kcal",
        "protein_grams",
        "fat_grams",
        "carbs_grams",
        "bulletpoint1",
        "bulletpoint2",
        "bulletpoint3",
        "recipe_diet_type_id",
        "season",
        "base",
        "protein_source",
        "preparation_time_minutes",
        "shelf_life_days",
        "equipment_needed",
        "origin_country",
        "recipe_cuisine",
        "in_your_box",
        "gousto_reference"
    ];

    //Example if we want to hide some of the fields
    protected $hidden = [
        'id'
    ];

    private $recipes;

    function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->recipes = json_decode(Cache::get('input'), true);
    }

    public function all($filters = [])
    {
        $page = array_get($filters, 'page', 1) - 1;
        $offset = array_get($filters, 'offset', 2);
        try{
            $this->recipes = $this->applyFilters($this->recipes, new RecipeFilter($filters));
            $this->recipes = $this->applyPagination($this->recipes, $page, $offset);
        } catch(NotFoundHttpException $e){
            throw new NotFoundHttpException('No recipes with your filters');
        }
        return $this->recipes;
    }
    public function find($id)
    {
        foreach ($this->recipes as $recipe) {
            if ($recipe['id'] != $id) {
                continue;
            }
            return new static($recipe);
        }
        throw new NotFoundHttpException('Receipe not found');
    }

    public function create($data)
    {
        try {
            $id = $this->getId();

            $recipe = new static($data);
            $recipe->setAttribute('id', (string)$id);

            array_push($this->recipes, $recipe->getAttributes());
            Cache::forever('input', json_encode($this->recipes));
        } catch(\Exception $e) {
            throw $e;
        }
        return $recipe;
    }

    public function update($data)
    {

    }

    public function toJson($options = 0)
    {
        // TODO: Implement toJson() method.
        return array_diff_key($this->attributes, array_flip($this->hidden));
    }

    /**
     * @param $max
     * @return mixed
     */
    private function getId()
    {
        $maxId = 0;
        foreach ($this->recipes as $recipe) {
            $maxId = (int)$recipe['id'] > $maxId ? (int)$recipe['id'] : $maxId;
        }
        return $maxId + 1;
    }


}