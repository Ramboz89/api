<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 29/04/16
 * Time: 01:04
 */

namespace App\Interfaces;


interface ITransformer
{
    public function transform(array $attributes);
} 