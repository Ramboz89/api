<?php

namespace App\Providers;

use App\Recipe;
use App\Repositories\Recipes\CSVRecipeRepository;
use App\Repositories\Recipes\IRecipeRepository;
use Illuminate\Support\ServiceProvider;

class GoustoServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IRecipeRepository::class, function ($app) {
            return new CSVRecipeRepository(
                new Recipe()
            );
        });
    }
}