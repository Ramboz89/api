<?php

namespace App\Http\Controllers;

use App\Repositories\Recipes\IRecipeRepository;
use App\Responses\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RecipeController extends Controller
{

    private $recipeRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IRecipeRepository $recipeRepository)
    {
        $this->recipeRepository = $recipeRepository;
    }

    public function index(Request $request)
    {
        try {
            $recipes = $this->recipeRepository->findAll($request->all());
            $response = new JsonResponse(['data' => $recipes]);
        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(['error' => $e->getMessage()], $e->getStatusCode());
        }

        return $response->send();
    }


    public function show($id)
    {
        try {
            $recipe = $this->recipeRepository->find($id);
            $response = new JsonResponse(['data' => $recipe]);
        } catch (NotFoundHttpException $e) {
            $response = new JsonResponse(['error' => $e->getMessage()], 404);
        }

        return $response->send();
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'title' => 'required',
                'slug' => 'required',
            ]);
            $data = $request->all();

            $recipe = $this->recipeRepository->store($data);
            $response = new JsonResponse(['data' => $recipe]);
        } catch (ValidationException $e) {
            $response = new JsonResponse(['error' => $e->getMessage()], 422);

        } catch (\Exception $e) {
            $response = new JsonResponse(['error' => $e->getMessage()], $e->getStatusCode());
        }

        return $response->send();

    }
}
