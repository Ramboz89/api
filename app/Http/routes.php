<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {

    $file=$app->storagePath()."/input.csv";
    $csv= file_get_contents($file);
    $rows = explode("\n", trim($csv));
    $data = array_slice($rows, 1);
    $keys = array_fill(0, count($data), $rows[0]);
    $json = array_map(function ($row, $key) {
        return array_combine(str_getcsv($key), str_getcsv($row));
    }, $data, $keys);
    unset($json[0]);
    $json = json_encode($json);
    \Illuminate\Support\Facades\Cache::forever('input', $json);
    return 'CSV in cache as JSON' . '<br>' . 'Available resource: /v1/recipes';
});

$app->group(['prefix' => 'v1'], function ($app) {
    $app->get('recipes', '\App\Http\Controllers\RecipeController@index');
    $app->post('recipes', '\App\Http\Controllers\RecipeController@store');
    $app->get('recipes/{id}','\App\Http\Controllers\RecipeController@show');
});


