<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 27/04/16
 * Time: 15:44
 */

namespace App\Filters;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QueryFilter
{
    protected $filters;
    /**
     * @var Builder
     */
    protected $results;
    protected $filterable = [
        'category'
    ];
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }
    public function apply(array $results)
    {
        $this->results = $results;
        $hasResults = false;
        foreach ($this->filters as $filter => $options) {
            if (in_array($filter, $this->filterable) && method_exists($this, $filter)) {
                $hasResults = $hasResults || call_user_func_array(
                    [$this, $filter],
                    array_filter([$options])
                );
            }
        }
        return $this->results;
    }

}