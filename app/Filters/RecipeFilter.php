<?php
/**
 * Created by PhpStorm.
 * User: marcobosso
 * Date: 27/04/16
 * Time: 16:09
 */

namespace App\Filters;


class RecipeFilter extends QueryFilter
{
    protected $filterable = [
        'category'
    ];
    public function category($value)
    {
        $values = explode(',', $value);
        foreach ($this->results as $row => $data) {
            if(!in_array(array_get($data, 'box_type'),$values)) {
                unset($this->results[$row]);
            }
        }
    }
}